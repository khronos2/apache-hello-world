Role Name
=========

Creates a RHEL 7.6 instance, stands up Apache/Firewalld, clones index.html from git repo, and then tests all tasks and content.

Requirements
------------

Boto and AWSCLI are required for this. Setup AWSCLI prior to executing.



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: apache-hello-world, x: 42 }

License
-------

BSD

Author Information
------------------

Justin B