import os
import requests
import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def test_httpd_is_installed(host):
    httpd = host.package("httpd")

    assert httpd.is_installed


def test_httpd_is_running(host):
    httpd = host.service("httpd")

    assert httpd.is_running
    assert httpd.is_enabled


def test_firewalld_is_installed(host):
    firewalld = host.package("firewalld")

    assert firewalld.is_installed


def test_firewalld_is_configured(host):
    with host.sudo():
        iptables = host.iptables.rules("filter", "IN_public_allow")
        http_rule_tcp = "-A IN_public_allow -p tcp -m tcp --dport 80"
        ssh_rule_tcp = "-A IN_public_allow -p tcp -m tcp --dport 22"

        assert http_rule_tcp in str(iptables)
        assert ssh_rule_tcp in str(iptables)


def test_firewalld_is_running(host):
    firewalld = host.service("firewalld")

    assert firewalld.is_running
    assert firewalld.is_enabled


def test_index_html_file_exists(host):
    index = host.file("/var/www/html/index.html")

    assert index.exists


def test_check_md5sum(host):
    index = host.file("/var/www/html/index.html")

    assert index.md5sum == 'ce7b3fa197acb355cf118ef1cb0f529e'


def test_verify_content():
    with open('/tmp/molecule/apache-hello-world/default/inventory/ansible_inventory.yml', 'r') as file:
        fi = file.read()

    re_ip = re.compile(r'\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b')
    ip = re.findall(re_ip, fi)

    r = requests.get('http://' + ip[0])
    content = "Hello World !"

    assert content in r.text
